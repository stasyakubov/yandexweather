import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.IOException;

public class Main {
    private String lat = "56", lon = "38",
            offset = "10800", name = "Europe/Moscow", abbr = "MSK",
            dst = "false", season = "autumn";
    private JSONObject weather;

    //Получить погоду
    private JSONObject getjson() throws IOException, JSONException {
        /*GET https://api.weather.yandex.ru/v1/forecast?
        lat=<широта>
        & lon=<долгота>
        & [lang=<язык ответа>]
        & [limit=<срок прогноза>]
        & [hours=<наличие почасового прогноза>]
        & [extra=<подробный прогноз осадков>]

        X-Yandex-API-Key: <значение ключа>*/

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("https://api.weather.yandex.ru/v1/forecast?lat=" + lat + "&lon=" + lon + "&lang=ru_RU&limit=2&hours=false");
        request.addHeader("X-Yandex-API-Key","a38acfff-9e52-46f7-83ef-60cb03578a9c\n");
        HttpResponse response = client.execute(request);
        return new JSONObject(EntityUtils.toString(response.getEntity()));
    }

    @BeforeSuite
    void before() throws IOException, JSONException {
        weather = getjson();
    }

    @Test(testName = "1. lat - широта (в градусах) соответствует заданной вами")
    void test1() throws JSONException {
        Assert.assertEquals(weather.getJSONObject("info").getString("lat"),lat);
    }

    @Test(testName = "2. lon - долгота (в градусах) соответствует заданной вами")
    void test2() throws JSONException {
        Assert.assertEquals(weather.getJSONObject("info").getString("lon"),lon);
    }

    @Test(testName = "3. offset - проверьте часовой пояс")
    void test3() throws JSONException {
        Assert.assertEquals(weather.getJSONObject("info").getJSONObject("tzinfo").getString("offset"),offset);
    }

    @Test(testName = "4. name - проверьте название часового пояса")
    void test4() throws JSONException {
        Assert.assertEquals(weather.getJSONObject("info").getJSONObject("tzinfo").getString("name"),name);
    }

    @Test(testName = "5. abbr - проверьте сокращенное название часового пояса")
    void test5() throws JSONException {
        Assert.assertEquals(weather.getJSONObject("info").getJSONObject("tzinfo").getString("abbr"),abbr);
    }

    @Test(testName = "6. dst - проверьте признак летнего времени")
    void test6() throws JSONException {
        Assert.assertEquals(weather.getJSONObject("info").getJSONObject("tzinfo").getString("dst"),dst);
    }

    @Test(testName = "7. url - проверьте страницу населенного пункта, убедитесь что ссылка правильная, что\n" +
            "широта и долгота внутри ссылки указаны верные")
    void test7() throws JSONException {
        Assert.assertEquals(weather.getJSONObject("info").getString("url"),"https://yandex.ru/pogoda/?lat=" + lat + "&lon=" + lon);
    }

    @Test(testName = "8. Проверьте длину прогноза, убедитесь что прогноз действительно на два дня")
    void test8() throws JSONException {

        Assert.assertEquals(weather.getJSONArray("forecasts").length(),2);
    }

    @Test(testName = "9. season - проверьте сезон")
    void test9() throws JSONException {
        Assert.assertEquals(weather.getJSONObject("fact").getString("season"),season);
    }

    @Test(testName = "10. Напишите логику и проверьте, что код фазы луны на второй день moon_code\n" +
            "соответсвует текстовому коду moon_text")
    void test10() throws JSONException {
        int moon_code = weather.getJSONArray("forecasts").getJSONObject(1).getInt("moon_code");
        String moon_text= "";
        switch (moon_code){
            case 0:
                moon_text = "full-moon";
                break;
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            case 7:
                moon_text = "decreasing-moon";
                break;
            case 4:
                moon_text = "last-quarter";
                break;
            case 8:
                moon_text = "new-moon";
                break;
            case 9:
            case 10:
            case 11:
            case 13:
            case 14:
            case 15:
                moon_text = "growing-moon";
                break;
            case 12:
                moon_text = "first-quarter";
                break;
        }
        Assert.assertEquals(weather.getJSONArray("forecasts").getJSONObject(1).getString("moon_text"),moon_text);
    }

}
